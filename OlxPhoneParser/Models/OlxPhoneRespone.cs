﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlxPhoneParser.Models
{
    public class OlxPhoneRespone
    {
        [JsonProperty("value")]
        public string Phone { get; set; }
    }
}
