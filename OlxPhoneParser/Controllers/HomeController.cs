﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OlxPhoneParser.Models;

namespace OlxPhoneParser.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Services.OlxPhoneParser _phoneParser;

        public HomeController(ILogger<HomeController> logger, Services.OlxPhoneParser phoneParser)
        {
            _logger = logger;
            _phoneParser = phoneParser;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetPhoneFromOrder([FromBody]string url)
        {
            var phone = await _phoneParser.GetPhoneFromOffer(url);
            return Ok(phone);
        }

        [HttpPost]
        public async Task<IActionResult> GetPhonesFromOffers([FromBody]string url)
        {
            var phoneNumbers = await _phoneParser.GetPhonesFromOffers(url);
            return Ok(phoneNumbers);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
