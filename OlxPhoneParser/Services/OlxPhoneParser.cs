﻿using AngleSharp;
using AngleSharp.Dom;
using Newtonsoft.Json;
using OlxPhoneParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OlxPhoneParser.Services
{
    public class OlxPhoneParser
    {
        private const string phoneTokenVariableRegex = @"var phoneToken = '[a-zA-Z0-9]+";
        private readonly HttpClient _httpClient;
        public OlxPhoneParser(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<PhoneResponse> GetPhoneFromOffer(string offerUrl)
        {
            var offerPageHttpResponse = await _httpClient.GetAsync(offerUrl);
            if (offerPageHttpResponse.StatusCode != HttpStatusCode.OK)
            {
                return new PhoneResponse()
                {
                    OfferUrl = offerUrl,
                    Phone = "Failed to get offer page."
                };
            }
            var responseContent = await offerPageHttpResponse.Content.ReadAsStringAsync();

            var offerId = GetIdFromOfferUrl(offerUrl);

            var phoneToken = GetPhoneToken(responseContent);

            if (string.IsNullOrEmpty(phoneToken))
            {
                return new PhoneResponse()
                {
                    OfferUrl = offerUrl,
                    Phone = "Phone token not found."
                };
            }

            var phoneReqUri = $"/ajax/misc/contact/phone/{offerId}/?pt={phoneToken}";
            Console.WriteLine();
            Console.WriteLine(phoneReqUri);
            Console.WriteLine();

            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, phoneReqUri);
            msg.Headers.Add("referer", offerUrl);
            msg.Headers.Add("x-requested-with", "XMLHttpRequest");
            _httpClient.DefaultRequestHeaders.Referrer = new Uri(offerUrl);

            var phoneHttpResponse = await _httpClient.SendAsync(msg);
            var phoneResponseText = await phoneHttpResponse.Content.ReadAsStringAsync();
            if (phoneHttpResponse.StatusCode != HttpStatusCode.OK)
            {
                return new PhoneResponse()
                {
                    OfferUrl = offerUrl,
                    Phone = $"Failed to get phone number (OfferId='{offerId}';PhoneToken='{phoneToken}';Server response:'{phoneResponseText}')"
                };
            }
            var phone = JsonConvert.DeserializeObject<OlxPhoneRespone>(phoneResponseText);
            return new PhoneResponse()
            {
                OfferUrl = offerUrl,
                Phone = phone.Phone
            };
        }
        public async Task<List<PhoneResponse>> GetPhonesFromOffers(string offersSearchUrl)
        {
            Console.WriteLine("Start processing olx search results...");
            var phoneNumbers = new List<PhoneResponse>();
            var pageNumber = 1;
            while (true)
            {
                Console.WriteLine($"Processing page #{pageNumber}\n");
                var response = await _httpClient.GetAsync($"{offersSearchUrl}?page={pageNumber}");
                if (response.StatusCode == HttpStatusCode.MovedPermanently)
                {
                    Console.WriteLine("\nProcessing completed.\n");
                    break;
                }

                var pageContent = await response.Content.ReadAsStringAsync();
                IBrowsingContext context = BrowsingContext.New(Configuration.Default);
                IDocument document = await context?.OpenAsync(x => x?.Content(pageContent ?? string.Empty));

                var offerUrlElements = document.GetElementsByClassName("marginright5 link linkWithHash detailsLink");

                foreach (IElement element in offerUrlElements)
                {
                    var offerUrl = element.GetAttribute("href");
                    var phone = await GetPhoneFromOffer(offerUrl);
                    phoneNumbers.Add(phone);
                    Console.WriteLine($"\tProccessed offer {offerUrl}.");
                    await Task.Delay(500);
                }
                pageNumber++;
            }
            return phoneNumbers;
        }

        private string GetPhoneToken(string pageContent)
        {
            Match match = Regex.Match(pageContent, phoneTokenVariableRegex, RegexOptions.Multiline);
            if (!match.Success)
            {
                return null;
            }
            var phoneToken = match.Value.Substring(match.Value.IndexOf('\'') + 1);
            return phoneToken;
        }
        private string GetIdFromOfferUrl(string offerUrl)
        {
            Uri uri = new Uri(offerUrl);
            if (string.IsNullOrEmpty(uri.AbsolutePath))
            {
                return null;
            }
            var id = uri.AbsolutePath.Split("-").Last().Replace(".html", "").Substring(2);
            return id;
        }
    }
}
