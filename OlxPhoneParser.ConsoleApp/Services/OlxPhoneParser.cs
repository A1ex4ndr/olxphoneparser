﻿using AngleSharp;
using AngleSharp.Dom;
using Newtonsoft.Json;
using OlxPhoneParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OlxPhoneParser.Services
{
    public class OlxPhoneParser
    {
        private const string phoneTokenVariableRegex = @"var phoneToken = '[a-zA-Z0-9]+";
        private readonly HttpClient _httpClient;
        public OlxPhoneParser(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<PhoneResponse> GetPhoneFromOffer(string offerUrl)
        {
            var offerPageHttpResponse = await _httpClient.GetAsync(offerUrl);
            if (offerPageHttpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Failed ({offerPageHttpResponse.StatusCode.ToString()}) {offerUrl}");
            }
            var responseContent = await offerPageHttpResponse.Content.ReadAsStringAsync();

            var offerId = GetIdFromOfferUrl(offerUrl);
            var phoneToken = GetPhoneToken(responseContent);

            if (string.IsNullOrEmpty(phoneToken))
            {
                throw new Exception($"Failed (TOKEN_NOT_FOUND) {offerUrl}");
            }

            var phoneReqPath = $"/ajax/misc/contact/phone/{offerId}/?pt={phoneToken}";

            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, phoneReqPath);
            msg.Headers.Add("referer", offerUrl);
            msg.Headers.Add("x-requested-with", "XMLHttpRequest");

            var phoneHttpResponse = await _httpClient.SendAsync(msg);
            var phoneResponseText = await phoneHttpResponse.Content.ReadAsStringAsync();
            if (phoneHttpResponse.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Failed ({phoneHttpResponse.StatusCode.ToString()}) {offerUrl}");
            }

            var phone = JsonConvert.DeserializeObject<OlxPhoneRespone>(phoneResponseText);
            if (phone.Phone.Contains("span"))
            {
                phone.Phone = await ProcessMultiplePhones(phone.Phone);
            }
            return new PhoneResponse()
            {
                OfferUrl = offerUrl,
                Phone = phone.Phone
            };
        }
        public async Task<List<PhoneResponse>> GetPhonesFromOffers(string offersSearchUrl)
        {
            Console.WriteLine("Start processing olx search results...");
            var phoneNumbers = new List<PhoneResponse>();

            var (phones, totalPages) = await GetPhonesOnPage(offersSearchUrl);
            Console.WriteLine($"TotalPages: {totalPages}");
            phoneNumbers.AddRange(phones);
            if (totalPages < 2)
            {
                return phoneNumbers;
            }

            var pageNumber = 2;
            while (pageNumber <= totalPages)
            {
                (phones, totalPages) = await GetPhonesOnPage(offersSearchUrl, pageNumber);
                phoneNumbers.AddRange(phones);
                pageNumber++;
            }
            return phoneNumbers;
        }

        private async Task<(List<PhoneResponse>, int)> GetPhonesOnPage(string offersSearchUrl, int pageNumber = 1)
        {
            var phoneNumbers = new List<PhoneResponse>();

            Console.WriteLine($"\nProcessing page #{pageNumber}\n");
            var urlQueryParamSignOrSeparator = offersSearchUrl.IndexOf('?') < 0 ? "?" : "&";
            var urlPageNumParam = pageNumber == 1 ? string.Empty : $"{urlQueryParamSignOrSeparator}page={pageNumber}";
            var response = await _httpClient.GetAsync($"{offersSearchUrl}{urlPageNumParam}");

            var pageContent = await response.Content.ReadAsStringAsync();
            IBrowsingContext context = BrowsingContext.New(Configuration.Default);
            IDocument document = await context?.OpenAsync(x => x?.Content(pageContent ?? string.Empty));

            var offerUrlElements = document.GetElementsByClassName("marginright5 link linkWithHash detailsLink");

            foreach (IElement element in offerUrlElements)
            {
                try
                {
                    var offerUrl = element.GetAttribute("href");
                    var phone = await GetPhoneFromOffer(offerUrl);
                    phoneNumbers.Add(phone);
                    Console.WriteLine($"\tProcessed offer {offerUrl}.");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("\t" + phone.Phone);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\t" + ex.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                await Task.Delay(1000);
            }
            var totalPages = GetTotalPages(document);
            return (phoneNumbers, totalPages);
        }

        private async Task<string> ProcessMultiplePhones(string phones)
        {
            IBrowsingContext context = BrowsingContext.New(Configuration.Default);
            IDocument document = await context?.OpenAsync(x => x?.Content(phones ?? string.Empty));
            phones = string.Join(',', document.QuerySelectorAll("span.block").Select(span => span.InnerHtml));
            return phones;
        }
        private string GetPhoneToken(string pageContent)
        {
            Match match = Regex.Match(pageContent, phoneTokenVariableRegex, RegexOptions.Multiline);
            if (!match.Success)
            {
                return null;
            }
            var phoneToken = match.Value.Substring(match.Value.IndexOf('\'') + 1);
            return phoneToken;
        }
        private string GetIdFromOfferUrl(string offerUrl)
        {
            Uri uri = new Uri(offerUrl);
            if (string.IsNullOrEmpty(uri.AbsolutePath))
            {
                return null;
            }
            var id = uri.AbsolutePath.Split("-").Last().Replace(".html", "").Substring(2);
            return id;
        }

        private int GetTotalPages(IDocument document)
        {
            var lastPageSpanElement = document.QuerySelectorAll(".item.fleft > a > span").LastOrDefault();
            if (lastPageSpanElement == null)
            {
                return 1;
            }
            if (!int.TryParse(lastPageSpanElement.InnerHtml, out int totalPages))
            {
                return 1;
            }
            return totalPages;
        }
    }
}
