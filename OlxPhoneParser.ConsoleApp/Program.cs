﻿using System;
using System.Net.Http;

namespace OlxPhoneParser.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
            httpClient.DefaultRequestHeaders.Add("authority", "www.olx.ua");
            httpClient.DefaultRequestHeaders.Add("accept", "*/*");
            httpClient.DefaultRequestHeaders.Add("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6");

            httpClient.BaseAddress = new Uri("https://www.olx.ua");

            Services.OlxPhoneParser parser = new Services.OlxPhoneParser(httpClient);

            AppDomain.CurrentDomain.ProcessExit += (sender, evt) =>
            {
                httpClient.Dispose();
            };

            new ConsoleApp(parser).Run();
        }
    }
}
