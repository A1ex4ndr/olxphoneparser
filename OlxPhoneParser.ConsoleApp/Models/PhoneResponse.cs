﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlxPhoneParser.Models
{
    public class PhoneResponse
    {
        public string Phone { get; set; }
        public string OfferUrl { get; set; }
    }
}
