﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace OlxPhoneParser.ConsoleApp
{
    class ConsoleApp
    {
        private readonly Services.OlxPhoneParser _parser;
        public ConsoleApp(Services.OlxPhoneParser parser)
        {
            _parser = parser;
        }

        public void Run()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    Console.Write("Вводи url: ");
                    var url = Console.ReadLine();
                    if (string.IsNullOrEmpty(url))
                    {
                        continue;
                    }
                    var urls = await _parser.GetPhonesFromOffers(url);
                    using (StreamWriter writer = new StreamWriter($"{DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss")}.txt", true))
                    {
                        urls.ForEach(phone => writer.WriteLine($"{phone.Phone} - {phone.OfferUrl}"));
                    }
                }
            }).Wait();
        }
    }
}
